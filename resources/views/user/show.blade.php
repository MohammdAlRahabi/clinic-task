@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Single User :
                        <div class="d-inline float-right">
                            <a href="{{route('home')}}">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                User id :
                            </div>
                            <div class="col-8">
                                {{$user->id}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                User Name :
                            </div>
                            <div class="col-8">
                                {{$user->name}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                User Email :
                            </div>
                            <div class="col-8">
                                {{$user->email}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
